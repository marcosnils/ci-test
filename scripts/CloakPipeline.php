<?php

class CloakPipeline {

  private function query($query) {
      $curl = curl_init('http://localhost:8080/');
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(['query' => $query]));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($curl);
      curl_close($curl);
      return $response;
  }

    public function getWorkdirFsid() {
        $workdirFsQuery = <<<EOF
        query {
            host {
                workdir {
                    read {
                        id
                    }
                }
            }
        }
        EOF;
        $response = $this->query($workdirFsQuery);
        $responseObj = json_decode($response);
        return $responseObj->data->host->workdir->read->id;
    }

    public function buildContainerWithDeps() {

        $workdirFsid = $this->getWorkdirFsid();
        $containerWithDepsQuery = <<<EOF
        query {
          core {
            image(ref: "php:8.1-apache") {
              exec(
                input: {mounts: [{fs: "{$workdirFsid}", path: "/mnt"}], workdir: "/var/www", args: ["sh", "-c", "cp -R /mnt/* /var/www/ && rm -rf /var/www/vendor"]}
              ) {
                fs {
                  exec(
                    input: {args: ["sh", "-c", "apt-get update && apt-get install -y curl git zip"], workdir: "/var/www"}
                  ) {
                    fs {
                      composer: exec(
                        input: {workdir: "/var/www", args: ["sh", "-c", "curl -sS https://getcomposer.org/installer | php && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer && composer install --no-scripts"]}
                      ) {
                        exitCode
                        stderr
                        stdout
                      }
                      apache: exec(
                        input: {workdir: "/var/www", args: ["sh", "-c", "sed -ri -e 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/*.conf && sed -ri -e 's!/var/www/!/var/www/public!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf && a2enmod rewrite"]}
                      ) {
                        exitCode
                        stderr
                        stdout
                      }
                    }
                  }
                }
              }
            }
          }        
        }
        EOF;
        
        $response = $this->query($containerWithDepsQuery);
        return json_decode($response);
    }
    
}

