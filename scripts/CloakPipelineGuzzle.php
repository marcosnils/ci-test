<?php
require_once '../vendor/autoload.php';

use GuzzleHttp\Client;

class CloakPipelineGuzzle {

    private $client;

    public function __construct() {
        $this->client = new Client([
            'base_uri' => 'http://localhost:8080/',
        ]);
    }

    private function getWorkdirFsid() {
        $workdirFsQuery = <<<EOF
        query {
            host {
                workdir {
                    read {
                        id
                    }
                }
            }
        }
        EOF;
        $response = $this->client->request('POST', '', [
            'form_params' => [
                'query' => $workdirFsQuery
            ]
        ]);
        $responseObj = json_decode($response->getBody()->getContents());
        return $responseObj->data->host->workdir->read->id;
    }

    public function buildContainerWithDeps() {

        $workdirFsid = $this->getWorkdirFsid();
        $containerWithDepsQuery = <<<EOF
        query {
          core {
            image(ref: "php:8.1-apache") {
              exec(
                input: {mounts: [{fs: "{$workdirFsid}", path: "/mnt"}], workdir: "/var/www", args: ["sh", "-c", "cp -R /mnt/* /var/www/ && rm -rf /var/www/vendor"]}
              ) {
                fs {
                  exec(
                    input: {args: ["sh", "-c", "apt-get update && apt-get install -y curl git zip"], workdir: "/var/www"}
                  ) {
                    fs {
                      exec(
                        input: {workdir: "/var/www", args: ["sh", "-c", "curl -sS https://getcomposer.org/installer | php && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer && composer install --no-scripts"]}
                      ) {
                        exitCode
                        stderr
                        stdout
                      }
                    }
                  }
                }
              }
            }
          }
        }
        EOF;
        
        $response = $this->client->request('POST', '', [
            'form_params' => [
                'query' => $containerWithDepsQuery
            ]
        ]);
        
        return $response->getBody()->getContents();
    }
    
}

