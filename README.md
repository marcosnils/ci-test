# run cloak script
cloak dev
docker run -it --rm --net=host -v $PWD:/var/www php:8.1-cli php /var/www/scripts/build.php

# OR 

# build container with Dockerfile
docker build . -t myapp.lumen
# run container, see app at http://localhost/
docker run --rm -v -p 80:80 myapp.lumen
