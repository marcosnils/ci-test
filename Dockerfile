FROM php:8.1-apache
COPY . /var/www
WORKDIR /var/www
RUN apt-get update && apt-get install -y curl git zip && \
  curl -sS https://getcomposer.org/installer | php && \
  chmod +x composer.phar && \
  mv composer.phar /usr/local/bin/composer && \
  composer install --no-scripts 
ENV APACHE_DOCUMENT_ROOT /var/www/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN sed -ri -e 's/80/${PORT}/g' /etc/apache2/sites-enabled/*.conf /etc/apache2/sites-available/*.conf /etc/apache2/ports.conf
RUN a2enmod rewrite

